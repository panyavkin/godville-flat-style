# Godville flat theme #

Flat theme for godville.net with smooth animations.


## How it use: ##

#### Userstyles ####
* Dark: [https://userstyles.org/styles/157528/godville-flat-theme-dark](https://userstyles.org/styles/157528/godville-flat-theme-dark)

* Light: [https://userstyles.org/styles/157529/godville-flat-theme-light](https://userstyles.org/styles/157529/godville-flat-theme-light)

#### Erinome Godville UI+ ####
Copy text from /build/userstyle-[dark|light].css in user CSS on UI+ settings page.


## Contacts ##

* Repo owner or admin
* Godville: [Daemonium](https://godville.net/gods/Daemonium)
* Email: [konstantin@panjavkin.ru](mailto:konstantin@panjavkin.ru)
* Telegram: [@Panyavkin](https://t.me/Panyavkin)


## License ##

[![License: CC BY 4.0](https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)

CC BY - Creative Commons Attribution
