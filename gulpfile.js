'use strict';

let gulp    = require('gulp4');
gulp.bin    = 'node_modules/.bin/gulp4';
const Growl = require('node-notifier').Growl;
let plugins = require('gulp-load-plugins')();

let path = {
	build: {
		css: 'build/'
	},
	src: {
		css: ['src/userstyle-light.scss', 'src/userstyle-dark.scss']
	},
	watch: {
		css: ['src/**/*.*', '!src/**/*.tmp']
	}
};


let notifier = new Growl({
	name: 'Builder gv-fds',
	host: '192.168.0.200',
	port: 23053
});


let notify = function(message) {
	return plugins.notify({
		onLast: true,
		notifier: function (options, callback) {
			notifier.notify({
				title: 'Build',
				message: message,
			});
			callback();
		}
	})
};


exports.css = function() {
	return gulp.src(path.src.css)
		.pipe(plugins.plumber({
			errorHandler: function (error) {
				console.log(error);
				this.emit('end');
			}
		}))
		.pipe(plugins.sourcemaps.init())
		.pipe(plugins.sass())
		.pipe(plugins.autoprefixer({
			browsers: ['last 4 versions']
		}))
		.pipe(plugins.cleanCss({
			level: 2
		}))
		.pipe(plugins.sourcemaps.write('.'))
		.pipe(gulp.dest(path.build.css))
		.pipe(plugins.touch())
		.pipe(notify('css builded'));
};



exports.globalWatcher = function(cb) {
	let tasks = ['css'];
	tasks.forEach(function(task) {
		gulp.watch(path.watch[task], {ignoreInitial: false}, function() {
			return exports[task]();
		});
	});
	return cb();
};


exports.default = function(cb) {
	let main;
	let spawn = require('child_process').spawn;
	gulp.watch('gulpfile.js', {usePolling: true, ignoreInitial: false}, function reload(cb) {
		if(main) main.kill();
		main = spawn(gulp.bin, ['globalWatcher'], {stdio: 'inherit'});
		return cb();
	});
	return cb();
};
